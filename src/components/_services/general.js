import Axios from 'axios';
import { setLocalObject, getLocalData } from "../utils/tools";
import { createProfile } from "./profile";
import { authSuccess, authFail, logout, navigateTo } from "../../store/actions/general";
import * as server from "../../resources/constants/api";
import * as settings from "../../resources/data/settings";
import * as architecture from "../../resources/data/architecture";
import * as addresses from "../../resources/data/addresses";
import * as structure from "../../resources/data/structure";
import * as structureCatalog from "../../resources/data/structure_catalog";


//########################### GET SETTINGS + ARCHITECTURE ###################################
//########################### GET SETTINGS + ARCHITECTURE ###################################
export async function getSettings() {
    
    // let results = {}
    // results['settings'] = settings.SETTINGS;
    // results['architecture'] = architecture.ARCHITECTURE;
    // results['addresses'] = addresses.ADDRESSES;
    // return results;

    // let results = {}
    // const token = await getLocalData('token');
    // let headers = {'headers': {'authorization': `Token ${token}`}};
    // if (token === '' || token === null){headers = {};}
    
    // results['settings'] = await Axios.get(server.URL_SERVER_DJANGO+"/useradmin/web", headers).then(res => {
    //     const temp = JSON.parse(res.data.results);
    //     return temp.settings;
    // }).catch(err => console.log(err))
    // results['architecture'] = await Axios.get(server.URL_SERVER_DJANGO+"/useradmin/architecture", headers).then(res => {
    //     const temp = JSON.parse(res.data.results);
    //     return temp.architecture;
    // }).catch(err => console.log(err));
    // return results;

};