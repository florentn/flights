import React from 'react';
import Flight from "./flight";

const Rotation = (props) => {
    return (  
        <div className={""} style={{}}>
            {props.rotation.map((flight,index)=>{
                return <Flight key={"flight_" + index} data={{...flight, ...{index: index}}} type={'remove'} valid={true} onSelected={(e) => props.onSelected(e)} />
            })
            }
        </div>    
    );
}
export default Rotation


