import React from 'react';
import Flight from "./flight";
import { checkValidity } from "../_services/planner";

const Flights = (props) => {

    return (  
        <div style={{height:'100%'}}>
            {props.flights.map((flight,index)=>{
                return <Flight key={"flight_" + index} data={{...flight, ...{index: index}}} type={'add'} valid={checkValidity(flight, props.rotation, props.location)} onSelected={(e) => props.onSelected(e)} />
                // if(checkValidity(flight, props.rotation, props.location))
                //     return <Flight key={"flight_" + index} data={{...flight, ...{index: index}}} type={'add'} valid={true} onSelected={(e) => props.onSelected(e)} />
            })
            }
        </div>    
    );
}
export default Flights

