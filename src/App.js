import React, { Component } from "react";
import { BrowserRouter } from "react-router-dom";
import CustomLayout from "./components/customLayout";
import BaseRouter from './components/routes';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css'; 
import history from './components/history';

export default class App extends Component {

  // ############################ RENDER ###################################
  // ############################ RENDER ###################################
  render() {  

        return (
          <BrowserRouter history={history} > 
            <ToastContainer />
              <CustomLayout>
              <BaseRouter /> 
            </CustomLayout>
          </BrowserRouter>
        );
  }
}
