import React from 'react';

const Flight = (props) => {
    
    return (
      <div className="card mb-1" >
        {/* TITLE + BUTTON */}
        <div className="float-left btn-light ">
          <div className="float-left col-lg-6 col-md-6 col-sm-6 col-6  ">
            <div className="card-title font-weight-bold btn" style={{margin: '5px !important'}}>{props.data.id}</div>
          </div>
          <div className="float-left col-lg-6 col-md-6 col-sm-6 col-6 btn">
            <button className={"btn btn-sm " + (props.type === 'add' ? 'btn-primary' : 'btn-danger')} onClick={() => props.onSelected({action: props.type, data:props.data})}>{(props.type === 'add' ? 'Add Flight' : 'Remove Flight')}</button>
          </div>
        </div>
        {/* DETAILS */}
        <div className="float-left p-2">
          <div className="float-left col-lg-6 col-md-6 col-sm-6 col-6  p-0">
            <div className="card-text font-weight-bold" style={{textAlign: 'center'}}>{props.data.origin} </div>
            <div className="card-text" style={{textAlign: 'center'}}>{props.data.readable_departure}</div>
          </div>
          <div className="float-left col-lg-6 col-md-6 col-sm-6 col-6 p-0">
            <div className="card-text font-weight-bold" style={{textAlign: 'center'}}>{props.data.destination} </div>
            <div className="card-text" style={{textAlign: 'center'}}>{props.data.readable_arrival}</div>
          </div>
        </div>

        {!props.valid && <div style={{position:'absolute', width:'100%', height:'100%', backgroundColor:'rgba(100,100,100,0.4)'}}></div>}
      </div>
    );
  }
export default Flight
