// import $ from 'jquery';


// ##################################  FIND INDEX ARRAY & OBJECT ####################################
// ##################################  FIND INDEX ARRAY & OBJECT ####################################
export function belongsTo(element, array) {
    let bBelongs = false;
    for(const key in array){ 
        if (array[key] === element){ bBelongs = true;}
    }
    return bBelongs;
}


// ############################ GET LOCAL DATA ###################################
// ############################ GET LOCAL DATA ###################################
export function getLocalObject(series) {
    try {
        let results = {}; 
        for (const key in series) {
            results= {...results,...{[key]: localStorage.getItem(key)}}
        }
        return results;
    } catch(e) {return 'error : ' + e;}
}

export function getLocalData(key) {
    try {
        const value = localStorage.getItem(key)
        if(value !== null) { return value; } else return null;
    } catch(e) { return 'error : ' + e; }
}

export function setLocalObject(series) {
    try { for (const key in series) { localStorage.setItem(key, series[key]);}
    } catch(e) {return 'error : ' + e;}
}

export async function setLocalData(key, value) {
    try { localStorage.setItem(key, value) } 
    catch (e) { console.log('error : ', e); }
}

export function deleteLocalObject(series) {
    try {
        for (const key in series) {
            localStorage.removeItem(key);
        }
        return getLocalObject(series);   
    } catch(e) {return 'error : ' + e;}
}

// ############################ GET TIME IN OBJECT ###################################
// ############################ GET TIME IN OBJECT ###################################
export function getTime() {
    const date = new Date();
    const time = {'hours': date.getHours(), 'minutes': date.getMinutes(), 'seconds': date.getSeconds()}
    return time;
}

// ############################ GET DATE IN SPECIFIC FORMAT ###################################
// ############################ GET DATE IN SPECIFIC FORMAT ###################################
export function getDate() {
    const today = new Date();
    const dd = today.getDate();
    const mm = today.getMonth()+1; 
    const yyyy = today.getFullYear();
    const hours = today.getHours();
    const minutes = today.getminutes();
    const seconds = today.getSeconds();

    const result = {year: yyyy, month: mm, day: dd, hour: hours, minute: minutes, second: seconds};
    return result;
}

// ############################ FORMAT DATE IN SPECIFIC FORMAT ###################################
// ############################ FORMAT DATE IN SPECIFIC FORMAT ###################################
export function formatDate(data, format) {
    const day = new Date(data);
    let result = '';
    switch(format){
        case 'dd/mm/yyL': result = (day.getDate()< 9 ? '0' + day.getDate() : day.getDate()) + '/' + (day.getMonth()< 9 ? '0' + parseInt(day.getMonth() + 1 ,10) : parseInt(day.getMonth() + 1 ,10)) + '/' + day.getFullYear().toString().substr(-2);break;
        case 'dd/mm/yyyyL': result = (day.getDate()< 9 ? '0' + day.getDate() : day.getDate()) + '/' + (day.getMonth()< 9 ? '0' + parseInt(day.getMonth() + 1 ,10) : parseInt(day.getMonth() + 1 ,10)) + '/' + day.getFullYear().toString();break;
        case 'yy/mm/ddL': result = day.getFullYear().toString().substr(-2) + '/' + (day.getMonth()< 9 ? '0' + parseInt(day.getMonth() + 1 ,10) : parseInt(day.getMonth() + 1 ,10)) + '/' + (day.getDate()< 9 ? '0' + day.getDate() : day.getDate());break;
        case 'yyyy/mm/ddL': result = day.getFullYear().toString() + '/' + (day.getMonth()< 9 ? '0' + parseInt(day.getMonth() + 1 ,10) : parseInt(day.getMonth() + 1 ,10)) + '/' + (day.getDate()< 9 ? '0' + day.getDate() : day.getDate());break;
        case 'yy-mm-ddL': result = day.getFullYear().toString().substr(-2) + '-' + (day.getMonth()< 9 ? '0' + parseInt(day.getMonth() + 1 ,10) : parseInt(day.getMonth() + 1 ,10)) + '-' + (day.getDate()< 9 ? '0' + day.getDate() : day.getDate());break;
        case 'yyyy-mm-ddL': result = day.getFullYear() + '-' + (day.getMonth()< 9 ? '0' + parseInt(day.getMonth() + 1 ,10) : parseInt(day.getMonth() + 1 ,10)) + '-' + (day.getDate()< 9 ? '0' + day.getDate() : day.getDate());break;
        case 'dd/mm/yy': result = (day.getUTCDate()< 9 ? '0' + day.getUTCDate() : day.getUTCDate()) + '/' + (day.getUTCMonth()< 9 ? '0' + parseInt(day.getUTCMonth() + 1 ,10) : parseInt(day.getUTCMonth() + 1 ,10)) + '/' + day.getUTCFullYear().toString().substr(-2);break;
        case 'dd/mm/yyyy': result = (day.getUTCDate()< 9 ? '0' + day.getUTCDate() : day.getUTCDate()) + '/' + (day.getUTCMonth()< 9 ? '0' + parseInt(day.getUTCMonth() + 1 ,10) : parseInt(day.getUTCMonth() + 1 ,10)) + '/' + day.getUTCFullYear().toString();break;
        case 'yy/mm/dd': result = day.getUTCFullYear().toString().substr(-2) + '/' + (day.getUTCMonth()< 9 ? '0' + parseInt(day.getUTCMonth() + 1 ,10) : parseInt(day.getUTCMonth() + 1 ,10)) + '/' + (day.getUTCDate()< 9 ? '0' + day.getUTCDate() : day.getUTCDate());break;
        case 'yyyy/mm/dd': result = day.getUTCFullYear().toString() + '/' + (day.getUTCMonth()< 9 ? '0' + parseInt(day.getUTCMonth() + 1 ,10) : parseInt(day.getUTCMonth() + 1 ,10)) + '/' + (day.getUTCDate()< 9 ? '0' + day.getUTCDate() : day.getUTCDate());break;
        case 'yy-mm-dd': result = day.getUTCFullYear().toString().substr(-2) + '-' + (day.getUTCMonth()< 9 ? '0' + parseInt(day.getUTCMonth() + 1 ,10) : parseInt(day.getUTCMonth() + 1 ,10)) + '-' + (day.getUTCDate()< 9 ? '0' + day.getUTCDate() : day.getUTCDate());break;
        case 'yyyy-mm-dd': result = day.getUTCFullYear() + '-' + (day.getUTCMonth()< 9 ? '0' + parseInt(day.getUTCMonth() + 1 ,10) : parseInt(day.getUTCMonth() + 1 ,10)) + '-' + (day.getUTCDate()< 9 ? '0' + day.getUTCDate() : day.getUTCDate());break;
        default:break;
    }
    return result;
}
