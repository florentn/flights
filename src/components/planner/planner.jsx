import React, { Component } from 'react';
import Aircrafts from "./aircrafts";
import Flights from "./flights";
import Rotation from "./rotation";
// import Summary from "./summary";
import { getPlanning, setPlanning, mapAircraftLocation, getFlights } from "../_services/planner";

let offset = 1050;
const limit = 25;

export default class Planner extends Component {
 
    //############################ CONSTRUCTOR ###################################
	//############################ CONSTRUCTOR ###################################
	constructor(props) {
        super(props);
        this.state = {flights: null, aircrafts:null, rotations: null, selectedAircraft: null, selectedRotation: null, selectedAircraftLocation: null, bSaved: true};
    }  

    async componentDidMount(){
        const data = await getPlanning();
        this.setState(data);
    }

    //############################ RENDER ###################################
    //############################ RENDER ###################################
    render() {
        if(this.state.flights){
            const { rotations, aircrafts, flights, selectedAircraft, selectedRotation, selectedAircraftLocation, bSaved } = this.state;

            return (
                <React.Fragment>
                <button className={"btn " + (bSaved ? "btn-primary" : "btn-danger")} disabled={bSaved} style={{position:'absolute', bottom: '10px', right: '10px', zIndex:10}} onClick={()=>{setPlanning(this.state.rotations);this.setState({bSaved: true})}}> {(bSaved ? "Rotations Saved" : "Save Rotations")}</button>
                <div className="float-left col-lg-12 col-md-12 col-sm-12 col-12 p-1" style={{height:'100vh', overflow:'none'}}>      
                    {/* 1ST COLUMN */}
                    <div className="float-left col-lg-4 col-md-4 col-sm-12 col-12 p-1" style={{height:'100%', overflow:'auto', display:'flex',flexDirection:'column'}}>      
                        <div className="font-weight-bold" style={{flex:1,textAlign:'center'}}>Select an Aircraft</div>
                        <div style={{flex:21}}>      
                            <Aircrafts aircrafts={aircrafts} rotations={rotations} selectedAircraft={selectedAircraft} onSelected={(e) => {this.handleAircraftSelection(e)}}/>
                        </div>
                    </div>
                    <React.Fragment>
                        {/* 2ND COLUMN */}
                        <div className="float-left col-lg-4 col-md-4 col-sm-12 col-12 p-1" style={{height:'100%', display:'flex',flexDirection:'column'}}>      
                            {/* <div style={{flex:1}}><Summary rotation={rotations[selectedAircraft]}/></div> */}
                            <div className="font-weight-bold" style={{flex:1,textAlign:'center'}}>All Flights</div>
                            <div style={{flex:20, overflow:'auto'}}>      
                            {selectedAircraft && <Flights flights={flights} rotation={selectedRotation} location={selectedAircraftLocation} onSelected={(e) => {this.handleFlightSelection(e)}} />}
                            </div>
                            <div style={{display: 'flex', flex:1, flexDirection: 'row', width: '100%', padding: '1%'}}>
                                <button disabled={offset === 0} className="btn btn-light" style={{flex:1}} onClick={()=>{this.loadFlights('prev')}}>Prev</button>
                                <button className="btn btn-light" style={{flex:1}} onClick={()=>{this.loadFlights('next')}}>Next</button>
                            </div>
                        </div>
                        {/* 3RD COLUMN */}
                        <div className="float-left col-lg-4 col-md-4 col-sm-12 col-12 p-1" style={{height:'100%', overflow:'auto', display:'flex',flexDirection:'column'}}>      
                            <div className="font-weight-bold" style={{flex:1,textAlign:'center'}}>{selectedAircraft} Rotation</div>
                            <div style={{flex:21}}>      
                            {selectedAircraft && <Rotation rotation={selectedRotation} onSelected={(e) => {this.handleFlightSelection(e)}} />}
                            </div>
                        </div>
                    </React.Fragment>
                </div>
                </React.Fragment>
            );
        } else return null;
    }

    //############################ EVENT HANDLERS ###################################
    //############################ EVENT HANDLERS ###################################
    handleAircraftSelection = (data) => {
        const rotation = this.state.rotations[data.ident] || [];
        const temp = mapAircraftLocation(data.base, rotation);
        this.setState({selectedAircraft: data.ident, selectedRotation: rotation, selectedAircraftLocation: temp});
    }

    handleFlightSelection = (data) => {
        let { rotations, selectedAircraft } = this.state;
        let tempRotation =  this.state.selectedRotation;
        let tempFlights =  this.state.flights;
        
        switch(data.action){
            default:break;
            case 'add':
                tempRotation = this.AddFlightToPool(data.data, tempRotation);
                tempFlights.splice(data.data.index, 1);
                break;
            case 'remove':
                tempFlights = this.AddFlightToPool(data.data, tempFlights);
                tempRotation.splice(data.data.index, 1);
            break;
        }

        const temp = mapAircraftLocation(null, tempRotation);
        rotations[selectedAircraft] = tempRotation;
        this.setState({rotations: rotations, selectedRotation: tempRotation, flights: tempFlights, selectedAircraftLocation: temp, bSaved: false});
    }


    //############################ MANAGES INSERT OF FLIGHT INTO SUBSET ACCORDING TO TIME ###################################
    //############################ MANAGES INSERT OF FLIGHT INTO SUBSET ACCORDING TO TIME ###################################
    AddFlightToPool = (flight, pool) =>{
        let bInserted = false;
        if(pool.length > 0){

            console.log('AddFlightToPool => ' , flight, ' => ', pool);
            
            pool.forEach((el, index) => {
                console.log('1  => ' , flight.departuretime, ' <= ', el.departuretime);
                if (flight.departuretime <= el.departuretime && bInserted === false){
                    pool.splice(index, 0, flight);
                    bInserted = true;
                }
                else { if (index == pool.length-1){
                    pool.push(flight);
                } }
            });
        } else {pool.push(flight);}                
        return pool;
    }

    //############################ MANAGES FLIGHTS LOADING FROM SERVICE ###################################
    //############################ MANAGES FLIGHTS LOADING FROM SERVICE ###################################
    loadFlights = async (action) =>{
        
        offset = (action === 'next' ? offset + limit : Math.max(0, offset - limit));
        const flights = await getFlights(offset);
        this.setState({flights: flights});
    }

}


