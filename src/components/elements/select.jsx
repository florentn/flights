import React, {Component} from "react";
import * as Fields from "../../resources/constants/select";

class Select extends Component {
  constructor(props) {
      super(props);
      this.val = '';

      switch(props.fields){
          case "font": this.options = Fields.FONTS; break;
          case "custom": this.options = props.list; break;
          default:break;
        }
  }


  handleChange = (e) => {
    this.val = e.target.value;
    // console.log('HANDLE CHANGE : '  , e.target.value);
    this.props.onChange(this.props.name, e.target.value);
  }


  render() {
    const { name, classes, value } = this.props;
    // console.log('SELECT : ', value);

    return (
      <div className={"NoOutline " + classes.options}>
      <select id={name} className={classes.container} value={value} onChange={e => this.handleChange(e)}>
        {/* <option value="" /> */}
        {this.options.map(option => (
          <option key={option['label'] + name} value={option['value']} data-attr={option['label']} >
            {option['label']}
          </option>
        ))}
      </select>
      </div>
    );
  }
}
export default Select;

