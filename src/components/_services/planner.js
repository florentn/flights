import Axios from 'axios';
import { setLocalData, getLocalData } from "./tools";
import * as server from "../../resources/constants/api";

const turnaround = 1200;

// ############################ GET PLANNING DATA : RAW AND USER SELECTED ROTATION ###################################
// ############################ GET PLANNING DATA : RAW AND USER SELECTED ROTATION ###################################
export async function getPlanning() {
  
    let results = {};
    
    // AIRCRAFTS ON REST API
    results['aircrafts'] = await Axios.get(server.URL_SERVER['aircrafts']).then(res => {
        return res.data.data;
    }).catch(err => console.log(err))
    // FLIGHTS ON REST API
    results['flights'] = await getFlights(1050);
    // ROTATIONS STORED LOCALLY
    results['rotations'] = JSON.parse(getLocalData('rotations')) || {};
    
    // console.log('GETPLANNING => ', results);
    return results;
}


// ############################ SAVES ROTATION DATA LOCALLY ###################################
// ############################ SAVES ROTATION DATA LOCALLY ###################################
export function setPlanning(data) {
      
    // console.log('SAVING ROTATIONS => ', data);
    setLocalData('rotations', JSON.stringify(data));
}

// ############################ SORT ARRAY ACCORDING TO DEPARTURE TIME (QUICKSORT) ###################################
// ############################ SORT ARRAY ACCORDING TO DEPARTURE TIME (QUICKSORT) ###################################
export function sortFlights(data) {
      
    if (data.length <= 1) { return data;} 
    else {
        let left = [], right = [], newArray = [];
        let pivot = data.pop(), length = data.length;
        
        for (let i = 0; i < length; i++) {
            if (data[i]['departuretime'] <= pivot['departuretime']) { left.push(data[i]); } 
            else {
                right.push(data[i]);
            }
        }
        return newArray.concat(sortFlights(left), pivot, sortFlights(right));
    }    
}


// ############################ GET PLANNING DATA : RAW AND USER SELECTED ROTATION ###################################
// ############################ GET PLANNING DATA : RAW AND USER SELECTED ROTATION ###################################
export async function getFlights(offset) {
  
    const url = server.URL_SERVER['flights'] + '?offset=' + offset;
    
    return await Axios.get(url).then(res => {
        // let result = res.data.data;
        // console.log('GETFLIGHTS 2 => ', res.data.data);
        let result = sortFlights(res.data.data)
        return result;
    }).catch(err => console.log(err));
    
}

//############################ CHECKS FLIGHT VALIDITY GIVEN CURRENT ROTATION ###################################
//############################ CHECKS FLIGHT VALIDITY GIVEN CURRENT ROTATION ###################################
export function checkValidity(flight, rotation, locations) {
    let bValid = true;

    // CHECKS TIME VALIDITY
    rotation.forEach(element => {  
        if ((flight.departuretime > element.departuretime && flight.departuretime < element.arrivaltime + turnaround) || 
            (flight.arrivaltime > element.departuretime && flight.arrivaltime < element.arrivaltime + turnaround) || 
            (flight.departuretime < element.departuretime && flight.arrivaltime > element.arrivaltime + turnaround)) {bValid = false;}
    });

    //CHECKS AEROPORT VALIDITY : WORKING BUT TOO RESTRICTIVE IN CURRENT FORM
    // if(bValid === true){
    //     locations.reverse().forEach(location => {  
    //         // console.log('PARTIAL LOCATION 1 => ', location.time , ' -> ', flight.departuretime, ' ==> ', location.location);
    //         if(location.time <=  flight.departuretime){
    //             if(flight.origin ===  location.location){
    //                 console.log('PARTIAL LOCATION 2 => ', location.location, ' == ', flight.origin);
    //                 bValid = true;
    //             }
    //                 else {bValid = false;}
    //             }
    //             // console.log('PARTIAL LOCATION 3 => ', flight.origin ===  location.location);
    //     });
    //     // console.log('FINAL LOCATION => ', bValid);
    //     // console.log('___________________');
    // }

    return bValid;
}

//############################ CHECKS FLIGHT VALIDITY GIVEN CURRENT ROTATION ###################################
//############################ CHECKS FLIGHT VALIDITY GIVEN CURRENT ROTATION ###################################
export function mapAircraftLocation(base, rotation) {
    let aircraftLocation = [{time:0, location:base}];
    if(rotation){
        rotation.forEach(element => {
            aircraftLocation.push({time:element.arrivaltime, location:element.destination});
        });
    }
    // console.log('mapAircraftLocation => ', aircraftLocation);
    return aircraftLocation;
}


//############################ CHECKS FLIGHT VALIDITY GIVEN CURRENT ROTATION ###################################
//############################ CHECKS FLIGHT VALIDITY GIVEN CURRENT ROTATION ###################################
export function getUtilisation(rotation){
    
    let timeInAir = 0;
    if(rotation){
        rotation.forEach(element => {
            timeInAir += element.arrivaltime - element.departuretime;
        });
    }
    return Math.trunc(timeInAir/86400*100);
}
