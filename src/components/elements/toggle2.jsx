import React, { Component } from 'react'
import BootstrapSwitchButton from 'bootstrap-switch-button-react'

class Toggle2 extends Component {

    constructor(props) {
        super(props);    
        this.state = { structure: {}, settings: props.settings, value: false};
    }
    
    render(){

        const {data, design,...otherProps} = this.props;

        return (
                <BootstrapSwitchButton
                checked={false}
                size="sm" style='w-100 mx-3'
                onlabel='Sign-in'
                onstyle='light'
                offlabel='Or Register'
                offstyle='success'
                onChange={e => {this.props.onChange(e)}}
                />
                );
    }

    
}
export default Toggle2;
