import React, { useState, useEffect } from 'react';
import { getUtilisation } from "../_services/planner";

const Aircraft = (props) => {
    
    let [cardBackgroundColor, setCardBackgroundColor] = useState("");
    useEffect(()=>{
        setCardBackgroundColor(props.selectedAircraft === props.data.ident ? 'btn-primary' : 'btn-light' );
    })
     
    return (
    <div className={"float-left p-2 " + cardBackgroundColor} style={{width:'100%',borderRadius:5, border:'solid lightgray 1px'}} onClick={() => props.onSelected(props.data)} >
        {/* <div className={"card mb-1 " + cardBackgroundColor} onClick={() => props.onSelected(props.data)} > */}
        {/* TITLE + BUTTON */}
        <div className="float-left col-lg-12 col-md-12 col-sm-12 col-12 p-0">
            <div className="float-left col-lg-6 col-md-6 col-sm-6 col-6 mb-1">
            <div className="font-weight-bold" style={{textAlign: 'center'}}>{props.data.ident}</div>
            </div>
            <div className="float-left col-lg-6 col-md-6 col-sm-6 col-6 mb-1">
                <div className="font-weight-bold" style={{textAlign: 'center', marginBottom: '5px !important'}}>{props.data.base}</div>
            </div>
        </div>
        {/* DETAILS */}
        <div className="float-left col-lg-12 col-md-12 col-sm-12 col-12 p-0">
            <div className="float-left col-lg-6 col-md-6 col-sm-6 col-6 p-0">
                <div className=" pl-4 pr-4" style={{width:'100%',textAlign: 'center'}}>{props.data.type}({props.data.economySeats}) </div>
            </div>
            <div className="float-left col-lg-6 col-md-6 col-sm-6 col-6 p-0" style={{display:'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                <div className="btn btn-success" >{getUtilisation(props.rotation)}%</div>
            </div>
        </div>

    </div>
    );
  }
  export default Aircraft