import React, { Component} from "react";
import { Route, Switch, Redirect  } from "react-router-dom";
import Planner from "./planner/planner";
// import { withRouter } from 'react-router-dom';

export default class BaseRouter extends Component {
  render() {    

      return (
        <Switch>
          <Route path="/planner" render={ () => {return <Planner /> }} />
          <Redirect from="/notFound " exact to="/home" />
          <Redirect from="/" exact to="/planner" />
      </Switch>
      // </div>
      );
  }
}
