import React from 'react';
import Aircraft from "./aircraft";

const Aircrafts = (props) => {

    return (  
        <div className={"float-left"} style={{width:'100%'}}>
            {props.aircrafts.map((aircraft,index)=>{
                return (
                        <Aircraft key={"aircraft_" + index} data={{...aircraft, ...{index: index}}} rotation={props.rotations[aircraft.ident]} selectedAircraft={props.selectedAircraft} onSelected={(e) => props.onSelected(e)} />
                    );
            })
            }
        </div>    
    );
}
export default Aircrafts

